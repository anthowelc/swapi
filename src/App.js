import React, { Component } from 'react'
import './App.css'
import SearchBox from './components/SearchBox'

class App extends Component {
  render () {
    return (
      <div className='App'>
        <h1>Star Wars Search Box</h1>
        <p className='App-intro'>
          To get started, type at least 2 letters.
        </p>
        <SearchBox />
      </div>
    )
  }
}

export default App
