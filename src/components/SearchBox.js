import React, { Component, Fragment } from 'react'
import { Input, List } from 'antd'

class SearchBox extends Component {
  state = {
    value: '',
    results: []
  }

  componentDidMount () {
    this.timer = null
  }

  handleChange = event => {
    const value = event.target.value
    this.setState({ value })

    clearTimeout(this.timer)

    this.timer = setTimeout(() => {
      this.setState({ results: [] })

      if (value.length >= 2) {
        this.fetchData(value)
      }
    }, 1000)

  }

  fetchData = async input => {
    const data = await fetch('https://swapi.co/api/?format=json')
    const json = await data.json()
    
    Object.keys(json).map(async categorie => {
      const data = await fetch(`https://swapi.co/api/${categorie}?search=${input}&format=json`)
      const json = await data.json()

      json.results.map(result => result.categorie = categorie)

      const results = [...this.state.results, ...json.results]
      this.setState({ results })
    })
  }

  render () {
    const list = this.state.results.length !== 0 ? (
      <List
        bordered
        dataSource={this.state.results}
        renderItem={item => (
          <List.Item>
            <List.Item.Meta
              title={item.name || item.title }
              description={item.categorie[0].toUpperCase() + item.categorie.slice(1)}
            />
            <div style={{ fontSize: '2em' }}>🢂</div>
          </List.Item>
        )}
      />
    ) : <Fragment />

    return (
      <Fragment>
        <Input
          value={this.state.value}
          onChange={this.handleChange}
          placeholder='people, planets, species, starships, vehicles...'
          type='text'
        />

        { list }

      </Fragment>
    )
  }
}

export default SearchBox
